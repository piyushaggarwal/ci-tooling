// Request a node to be allocated to us
node( currentPlatform ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Our CI scripts
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/ci-tooling']]
			]

			// Projects metadata and next generation dependency metadata
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/repo-metadata/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/sysadmin/repo-metadata']]
			]

			// Dependency Metadata
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/kde-build-metadata/']],
				userRemoteConfigs: [[url: 'https://anongit.kde.org/kde-build-metadata']]
			]

		}

		// Now we run the Product Dependency Build Process
		stage('Build Product Dependencies') {
			// This script will do the following:
			// 1) Determine what is in this Product
			// 2) Determine what those repositories depend on
			// 3) Determine what dependencies are outside of this Product
			// 4) Sort those dependencies into an appropriate order to build them
			// 5) Checkout, Configure, Compile, Install and Capture the Installation each of those dependencies in turn
			// We can't do this as Pipeline steps unfortunately (at least not easily)
			// Tests and Other Quality Tests won't be run during this process
			// The results of this process are only intended to be used as part of the base of this Product, so don't need testing
			sh "python3 -u ci-tooling/helpers/build-product-dependencies.py --product ${productName} --branchGroup ${branchGroup} --environment ${ciEnvironment} --platform ${currentPlatform} --installTo '$HOME/install-prefix/'"
		}

	}

	// As the FreeBSD Slaves are permanent ones, we erase the Workspace
	deleteDir()
	// Along with the installation prefix, which lives outside the Workspace
	sh "rm -rf $HOME/install-prefix/"

	// Let's determine if we need to send out notifications
	// What happened in our previous build?
	def previousResult = currentBuild.previousBuild?.result
	// If our condition has changed, is FAILURE or UNSTABLE then we want to send an email
	if( previousResult != currentBuild.result || currentBuild.result == 'FAILURE' || currentBuild.result == 'UNSTABLE' ) {
		// Construct the list of our recipients - these people always want to be notified about Dependency Build jobs
		def mailTo = []
		// If someone kicked this job off, they're presumably interested as well
		mailTo << emailextrecipients( [[$class: 'RequesterRecipientProvider']] )
		// Finalise the list of recipients
		mailTo = mailTo.join(',')

		// Send the email now
		emailext(
			to: mailTo,
			body: '${JELLY_SCRIPT,template="html_gmail"}',
			mimeType: 'text/html',
			subject: 'KDE CI: ${PROJECT_NAME} - Build # ${BUILD_NUMBER} - ${BUILD_STATUS}!', 
			attachLog: false
		)
	}
}
}
